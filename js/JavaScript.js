const btnWrapper = document.querySelector('.btn-wrapper');
let previousBtn;
const keyBoard = {
    Enter: 'Enter',
    s: 's',
    e: 'e',
    o: 'o',
    n: 'n',
    l: 'l',
    z: 'z',
}

btnWrapper.addEventListener('click', (e) => {
    if (e.target !== e.currentTarget && e.target.classList.contains('btn')) {
        if (previousBtn) previousBtn.classList.remove('blue');
        e.target.classList.add('blue');
        previousBtn = e.target;
    }
})

document.addEventListener('keydown', (e) => {
    if (keyBoard[`${e.key}`]) {
        if (previousBtn) previousBtn.classList.remove('blue');
        document.querySelector(`[data-key="${e.key}"]`).classList.add('blue');
        previousBtn = document.querySelector(`[data-key="${e.key}"]`);
    }
})
